### Explicação do Projeto

O projeto foi desenvolvido usando primariamente as extensões do Quarkus.

Para o retorno das informações em formato JSON, foram utilizadas as extensões `quarkus-resteasy` e `quarkus-resteasy-jsonb`.

Para a conexão com o banco de dados MySQL, foram utilizadas as extensões `quarkus-hibernate-orm` e `quarkus-hibernate-orm-panache`, e o driver `quarkus-jdbc-mysql`.

Para a autenticação Basic foi utilizada a extensão `quarkus-elytron-security-properties-file`, com as informações dos usuários inseridas diretamente no arquivo `application.properties`.

A comunicação com a API do Google Books foi realizada através das bibliotecas disponibilizadas pelo Google, `google-api-services-books` e `google-http-client-gson`.

### Instruções para Uso

O projeto pode ser executado em modo de desenvolvimento pelo comando `./mvnw compile quarkus:dev`, ou executado através do JAR gerado em `target/java-quarkus-1.0-SNAPSHOT-runner.jar`.

A autenticação no servidor deve ser realizada de modo Basic, com um dos usuários disponibilizados: `joao` ou `jose` (para ambos, a senha é igual ao login).

A API contém os seguintes endpoints:

- `GET /app/books?q=query&p=1` para realizar uma consulta na listagem de livros do Google Books. O parâmetro `q` é obrigatório, o parâmetro `p` (paginação) é opcional, mas se presente deve ser um número inteiro. Este endpoint pode ser acessado sem autenticação, nesse caso não terá a informação de livros favoritos.
- `GET /app/with-stars&p=1` para consultar os livros favoritos do usuário. O parâmetro `p` (paginação) é opcional, mas se presente deve ser um número inteiro.
- `POST /app/books/{id}/favorite` para adicionar um livro à lista de favoritos do usuário.
- `DELETE /app/books/{id}/favorite` para remover um livro da lista de favoritos do usuário.

### Detalhes do Desenvolvimento

O projeto está claramente incompleto, frente aos requisitos solicitados.

As funcionalidades estão todas implementadas e funcionando corretamente, mas muitas validações de consistência de dados de entrada e retorno foram omitidas por questão de tempo. A chamada do endpoint `GET /app/books` possui validações de entrada como exemplo.

Criei apenas testes unitários básicos para as validações de entrada do endpoint `GET /app/books`. Tenho pouca experiência com testes unitários, e não tenho noção de como fazer testes unitários efetivos em um projeto integrado como este.

Não foi criada imagem Docker pois não tenho experiência com isso. O Quarkus gera um Dockerfile padrão, mas acredito que seja insuficiente para o projeto visto a necessidade de se instalar e configurar também o banco de dados.

Como aprendizado, fiquei surpreso com o quão poderoso e fácil de utilizar é o Quarkus. Não conhecia o projeto antes de realizar este teste, mas através de seus guias foi fácil de compreender o funcionamento básico do projeto e suas extensões.

Estimo ter utilizado aproximadamente 13~14h para o desenvolvimento desde projeto teste.
