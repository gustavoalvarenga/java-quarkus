package net.lyncas.books.service;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.books.Books;
import com.google.api.services.books.model.Volume;
import com.google.api.services.books.model.Volumes;
import net.lyncas.books.model.Book;
import net.lyncas.books.persistence.Favorite;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class BooksService {

    private static final String LITE = "lite";
    private static final String KEY = "AIzaSyADW3zaCb0OdHzsnkD9QhDz3YvfbbjyneU";

    private final Books books;

    public BooksService() {
        try {
            books = new Books.Builder(GoogleNetHttpTransport.newTrustedTransport(), new GsonFactory(), null).build();
        } catch (GeneralSecurityException | IOException e) {
            throw new RuntimeException("Could not initialize the Books API.", e);
        }
    }

    public List<Book> listBooks(String userName, String query, Long page) {
        List<Book> listBooks = null;

        try {
            Volumes volumes = books.volumes().list(query).setStartIndex((page - 1) * 10).setKey(KEY).setProjection(LITE).execute();

            listBooks = volumes.getItems().stream().map(volume -> new Book(volume.getId(),
                    volume.getVolumeInfo().getTitle(),
                    getAuthorFromVolumeInfo(volume),
                    userName != null && Favorite.isFavorite(volume.getId(), userName))).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return listBooks;
    }

    public List<Book> getFavorites(String userName, Integer page) {
        return Favorite.getUserFavorites(userName, page).stream().map(favorite -> {
            Book book = new Book();
            book.id = favorite.bookId;
            book.favorite = true;
            try {
                Volume volume = books.volumes().get(favorite.bookId).setKey(KEY).setProjection(LITE).execute();
                book.name = volume.getVolumeInfo().getTitle();
                book.author = getAuthorFromVolumeInfo(volume);
            } catch (IOException e) {
                book.name = "Book not found";
                book.author = "Unknown Author";
            }
            return book;
        }).collect(Collectors.toList());
    }

    @Transactional
    public void addFavorite(String userName, String bookId) {
        if (!Favorite.isFavorite(bookId, userName)) {
            Favorite fav = new Favorite();
            fav.bookId = bookId;
            fav.userName = userName;
            fav.persist();
        }
    }

    @Transactional
    public void deleteFavorite(String userName, String bookId) {
        Favorite.deleteFavorite(bookId, userName);
    }

    private String getAuthorFromVolumeInfo(Volume volume) {
        return volume.getVolumeInfo().getAuthors() != null ? String.join(", ", volume.getVolumeInfo().getAuthors()) : "Unknown Author";
    }
}
