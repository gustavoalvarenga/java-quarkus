package net.lyncas.books.persistence;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.panache.common.Page;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

@Entity
public class Favorite extends PanacheEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "favoriteSeq")
    public Long id;
    public String bookId;
    public String userName;

    public static boolean isFavorite(String bookId, String userName) {
        return find("bookId = ?1 and userName = ?2", bookId, userName).count() > 0;
    }

    public static void deleteFavorite(String bookId, String userName) {
        delete("bookId = ?1 and userName = ?2", bookId, userName);
    }

    public static List<Favorite> getUserFavorites(String userName, Integer page) {
        return find("userName", userName).page(Page.of(page - 1, 10)).list();
    }
}
