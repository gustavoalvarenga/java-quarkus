package net.lyncas.books.resource;

import io.quarkus.security.Authenticated;
import net.lyncas.books.model.Book;
import net.lyncas.books.model.ErrorResponse;
import net.lyncas.books.service.BooksService;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;
import java.util.List;

@Path("/app")
public class BooksResource {

    @Inject
    BooksService googleBooksService;

    @GET
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/books")
    public Response books(@Context SecurityContext ctx, @QueryParam("q") String query, @DefaultValue("1") @QueryParam("p") String pageParam) {
        Response response;
        if (query == null || query.isBlank()) {
            response = Response.ok(new ErrorResponse("The 'q' param is required.")).build();
        } else {
            String name = getCallerName(ctx);
            try {
                Long page = Long.parseLong(pageParam);
                response = Response.ok(googleBooksService.listBooks(name, query, page)).build();
            } catch (NumberFormatException e) {
                response = Response.ok(new ErrorResponse("The 'p' param must be a number.")).build();
            }
        }
        return response;
    }

    @GET
    @Authenticated
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/with-stars")
    public List<Book> getFavorites(@Context SecurityContext ctx, @DefaultValue("1") @QueryParam("p") Integer page) {
        String name = getCallerName(ctx);
        return googleBooksService.getFavorites(name, page);
    }

    @POST
    @Authenticated
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/books/{id}/favorite")
    public Response addFavorite(@Context SecurityContext ctx, @PathParam("id") String id) {
        String name = getCallerName(ctx);
        googleBooksService.addFavorite(name, id);
        return Response.ok().build();
    }

    @DELETE
    @Authenticated
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/books/{id}/favorite")
    public Response deleteFavorite(@Context SecurityContext ctx, @PathParam("id") String id) {
        String name = getCallerName(ctx);
        googleBooksService.deleteFavorite(name, id);
        return Response.ok().build();
    }

    private String getCallerName(SecurityContext ctx) {
        Principal caller = ctx.getUserPrincipal();
        return caller == null ? null : caller.getName();
    }
}
