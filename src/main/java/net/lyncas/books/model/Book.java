package net.lyncas.books.model;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class Book {

    public String id;
    public String name;
    public String author;
    public boolean favorite;

    public Book() {
    }

    public Book(String id, String name, String author, boolean favorite) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.favorite = favorite;
    }

}
