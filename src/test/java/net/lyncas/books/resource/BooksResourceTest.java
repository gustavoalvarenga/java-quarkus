package net.lyncas.books.resource;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

@QuarkusTest
public class BooksResourceTest {

    @Test
    public void testBooksEndpointNoQuery() {
        given()
                .when().get("/app/books")
                .then()
                .statusCode(200)
                .body(containsString("The 'q' param is required."));
    }

    @Test
    public void testBooksEndpointInvalidPage() {
        given()
                .when().get("/app/books?q=test&p=a")
                .then()
                .statusCode(200)
                .body(containsString("The 'p' param must be a number."));
    }

}
